# Contains relative paths for all resources used
# If inserting a new song, please follow the naming convention:
# Spaces are replaced with _ and the name of song and artist are seprated by :
songs = ["../resources/music/Chaotic:Waterflame.mp3",
         "../resources/music/Glorious_Morning_2:Waterflame.mp3",
         "../resources/music/Glorious_Morning:Waterflame.mp3",
         "../resources/music/Take_you_Home_Tonight:Vibe_Tracks.mp3",
         "../resources/music/The_Maze:Nightkilla.mp3",
         "../resources/music/Victory:Two_Steps_from_Hell.mp3"]
void = ["../resources/images/water.jpg",
        "../resources/images/road.jpg",
        "../resources/images/grassland.jpg",
        "../resources/images/space.jpg"]
partition = ["../resources/images/land.jpg",
             "../resources/images/grass.jpg",
             "../resources/images/corridors.jpg",
             "../resources/images/spacestation.jpg"]
instructions = ["../resources/images/instructions0.jpg",
                "../resources/images/instructions1.jpg",
                "../resources/images/instructions2.jpg",
                "../resources/images/instructions3.jpg"]
player = ["../resources/images/red.jpg",
          "../resources/images/orange.jpg",
          "../resources/images/gold.jpg",
          "../resources/images/mint.jpg",
          "../resources/images/purple.jpg",
          "../resources/images/yellow.jpg",
          "../resources/images/white.jpg"]
fixobs = ["../resources/images/rock.jpg",
          "../resources/images/bandit.jpg",
          "../resources/images/reaper.jpg",
          "../resources/images/radiation.jpg"]
obslef = ["../resources/images/boatl.jpg",
          "../resources/images/carl.jpg",
          "../resources/images/dragonl.jpg",
          "../resources/images/asteriod.jpg"]
title_bg = "../resources/images/title_bg.jpg"
obsrig = ["../resources/images/boatr.jpg",
          "../resources/images/carr.jpg",
          "../resources/images/dragonr.jpg",
          "../resources/images/asteriod.jpg"]
# initilizing fonts
myfont = ['Comic Sans MS', 75]
myfont2 = ['Comic Sans MS', 40]

