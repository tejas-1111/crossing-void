import pygame
from pygame.locals import *
import math
import random
from time import sleep
import config

# Initiating pygame
pygame.init()
# Defining the screen dimensions
width, height = 1920, 1080
screen = pygame.display.set_mode((width, height))
# Variable used to select things like background, player color
game_type = 0
enviroment_type = 0
player1_type = 0
player2_type = 0
# Main game loop
while 1:
    game_type = 0
    pygame.mixer.music.stop()
    instructions = pygame.image.load(config.instructions[game_type])
    screen.fill(0)
    screen.blit(instructions, (500, 100))
    pygame.display.flip()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit(0)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_KP8:
                game_type = 1
                # Execute selector for selecting elements
                exec(open("selector.py").read())
                # Executing the single player py file
                exec(open("1_player.py").read())
            elif event.key == pygame.K_KP5:
                # execfile("1_player.py") or python 2
                game_type = 2
                # Execute selector for selecting elements
                exec(open("selector.py").read())
                # Executing the double player py file
                exec(open("2_players_1_mover.py").read())
            elif event.key == pygame.K_KP2:
                # execfile("2_players_2_movers.py") or python 2
                game_type = 3
                # Execute selector for selecting elements
                exec(open("selector.py").read())
                # Executing the double player py file
                exec(open("2_players_2_movers.py").read())
            elif event.key == pygame.K_KP0:
                pygame.quit()
                exit(0)

