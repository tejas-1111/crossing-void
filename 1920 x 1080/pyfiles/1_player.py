import config


# Setting up all global variables
songs = config.songs
# Pygame not defined as this file will be executed by main menu
SONG_END = pygame.USEREVENT + 1
pygame.mixer.music.set_endevent(SONG_END)
pygame.mixer.music.load(songs[0])
pygame.mixer.music.play()
music_index = 1
paused = False
# enviroment_type, player1_type, player2_type, game_type defined in main menu
void = pygame.image.load(config.void[enviroment_type])
partition = pygame.image.load(config.partition[enviroment_type])
instructions = pygame.image.load(config.instructions[game_type])
player1 = pygame.image.load(config.player[player1_type])
fixobs = pygame.image.load(config.fixobs[enviroment_type])
obslef = pygame.image.load(config.obslef[enviroment_type])
title_bg = pygame.image.load(config.title_bg)
obsrig = pygame.image.load(config.obsrig[enviroment_type])
myfont = pygame.font.SysFont(config.myfont[0], config.myfont[1])
myfont2 = pygame.font.SysFont(config.myfont2[0], config.myfont2[1])
round_type = 1
keys = [False, False, False, False]
pos = [900, 850]
alive = True
fixed_obstacles = []
moving_obstacles = []
master_counter = 100
slave_counter = -1
speed = 5
score = 0
time_keeper = 0
directed_score = 0
game = True


# For playing next Track
def Next_Song():
    global songs, music_index, paused
    pygame.mixer.music.load(songs[music_index])
    pygame.mixer.music.play()
    if paused:
        pygame.mixer.music.pause()
    music_index += 1
    music_index %= len(songs)


# For playing previous Track
def Prev_Song():
    global songs, music_index, paused
    music_index += len(songs)-2
    music_index %= len(songs)
    pygame.mixer.music.load(songs[music_index])
    pygame.mixer.music.play()
    if paused:
        pygame.mixer.music.pause()
    music_index += 1
    music_index %= len(songs)


# For Pausing/Unpausing Track
def Pause_Song():
    global paused
    if not paused:
        pygame.mixer.music.pause()
    else:
        pygame.mixer.music.unpause()
    paused = not paused


# Displays instructions for this game
def Instruction_Screen():
    screen.fill(0)
    screen.blit(instructions, (500, 100))
    pygame.display.flip()
    while 1:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                Continue()
                return
            if event.type == pygame.QUIT:
                pygame.quit()
                exit(0)


# The continue screen between two rounds
def Continue():
    global keys, moving_obstacles, fixed_obstacles, time_keeper, round_type
    global directed_score
    # Updating values required
    keys = [False, False, False, False]
    moving_obstacles = []
    fixed_obstacles = []
    round_type = 1 - round_type
    directed_score = 0
    screen.fill(0)
    pygame.display.flip()
    start_ticks = pygame.time.get_ticks()
    time_keeper = start_ticks
    seconds = (pygame.time.get_ticks()-start_ticks)/1000
    while seconds < 3:
        screen.fill(0)
        textsurface1 = myfont.render('Game starts in:', False, (255, 255, 255))
        screen.blit(textsurface1, (750, 300))
        text2 = myfont.render(str(int(4-seconds)), False, (255, 255, 255))
        screen.blit(text2, (925, 400))
        pygame.display.flip()
        seconds = (pygame.time.get_ticks()-start_ticks)/1000
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit(0)
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_i:
                    Prev_Song()
                if event.key == pygame.K_o:
                    Pause_Song()
                if event.key == pygame.K_p:
                    Next_Song()
    Fixed_Obstacles()


# Creates fixed obstacles and the inital moving obstacles
def Fixed_Obstacles():
    global fixed_obstacles, moving_obstacles
    # Fixed obstacles
    number = random.randint(15, 25)
    i = 0
    while i < number:
        x = random.randint(0, 31)
        y = random.randint(1, 5)
        flag = True
        if len(fixed_obstacles) == 0:
            fixed_obstacles.append([x, y])
            i += 1
            continue
        for obstacle in fixed_obstacles:
            if x == obstacle[0] and y == obstacle[1]:
                flag = False
        if flag:
            fixed_obstacles.append([x, y])
            i += 1
    i = 0
    # Moving obstacles
    number = random.randint(10, 20)
    while i < number:
        x = random.randint(0, 23)
        y = random.randint(0, 5)
        flag = True
        if len(moving_obstacles) == 0:
            moving_obstacles.append([x*60, 170 + y*120, y])
            i += 1
            continue
        for obstacle in moving_obstacles:
            if abs(obstacle[0]-x*60) < 80 and y == obstacle[2]:
                flag = False
        if flag:
            moving_obstacles.append([x*60, 170 + y*120, y])
            i += 1


# Used to return a range for a for loop with custom parameters
def my_range(start, end, step):
    while start <= end:
        yield start
        start += step


# Makes the basic graphics of the game
def Background():
    global alive, score, songs, music_index, paused
    screen.fill(0)
    # Partitions
    for y in my_range(110, 831, 120):
        for x in my_range(0, 1920, 60):
            screen.blit(partition, (x, y))
    # Voids
    for y in my_range(170, 771, 120):
        for x in my_range(0, 1920, 60):
            screen.blit(void, (x, y))
    # Player
    if alive:
        screen.blit(player1, pos)
    # Fixed objects
    for i in range(0, len(fixed_obstacles)):
        x = fixed_obstacles[i][0]
        y = fixed_obstacles[i][1]
        screen.blit(fixobs, (x*60, 110+y*120))
    # Title, score and muisc bar
    screen.blit(title_bg, (0, 0))
    screen.blit(title_bg, (0, 891))
    text1 = myfont2.render('Score: '+str(score), False, (255, 255, 255))
    screen.blit(text1, (10, 78))
    text4 = myfont.render('Crossing Void', False, (224, 231, 34))
    screen.blit(text4, (800, 12))
    temp = 'Music : '
    if paused:
        temp += "Paused"
    else:
        temp += "Playing"
    text5 = myfont2.render(temp, False, (255, 255, 255))
    screen.blit(text5, (800, 900))
    temp = songs[(music_index+len(config.songs)-1) % len(config.songs)]
    temp = temp[19:]
    temp = temp.replace("_", " ")
    temp = temp.replace(":", " By ")
    temp = temp.replace(".mp3", "")
    text6 = myfont2.render(temp, False, (255, 255, 255))
    screen.blit(text6, (800, 930))


# Handles all events in the game
def Events():
    global keys
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit(0)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                keys[0] = True
            if event.key == pygame.K_a:
                keys[1] = True
            if event.key == pygame.K_s:
                keys[2] = True
            if event.key == pygame.K_d:
                keys[3] = True
            if event.key == pygame.K_i:
                Prev_Song()
            if event.key == pygame.K_o:
                Pause_Song()
            if event.key == pygame.K_p:
                Next_Song()
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_w:
                keys[0] = False
            if event.key == pygame.K_a:
                keys[1] = False
            if event.key == pygame.K_s:
                keys[2] = False
            if event.key == pygame.K_d:
                keys[3] = False
        if event.type == SONG_END:
            Next_Song()


# Makes moving obstacles
def Moving_Obstacles():
    global master_counter, moving_obstacles, slave_counter
    master_counter -= 1
    if master_counter == 0:  # timer for a new obstacle spawner
        sflag = True
        while sflag:
            sflag = False
            row = random.randint(0, 5)
            # Determines direction and postion
            if len(moving_obstacles) == 0:
                if row % 2 == 0:
                    moving_obstacles.append([-80, 170+row*120, row])
                else:
                    moving_obstacles.append([1920, 170+row*120, row])
                continue
            for obstacle in moving_obstacles:
                if row % 2 == 0:
                    if obstacle[0] < 0 and row == obstacle[2]:
                        sflag = True
                else:
                    if obstacle[0] > 1841 and row == obstacle[2]:
                        sflag = True
            if row % 2 == 0 and not sflag:
                moving_obstacles.append([-80, 170+row*120, row])
            elif not sflag:
                moving_obstacles.append([1920, 170+row*120, row])
        # Updating timers
        master_counter = 100 - (slave_counter * 2)
        if slave_counter >= 40:
            slave_counter = 40
        else:
            slave_counter += 5
    index = 0
    # Removing obstacles which went out of screen
    for obstacle in moving_obstacles:
        if obstacle[0] > 1920 or obstacle[0] < -80:
            moving_obstacles.pop(index)
        else:
            if obstacle[2] % 2 == 0:
                obstacle[0] += speed
            else:
                obstacle[0] -= speed
        index = index + 1
    # Prints all moving obstacles
    for obstacle in moving_obstacles:
        if obstacle[2] % 2 == 0:
            screen.blit(obsrig, (obstacle[0], obstacle[1]))
        else:
            screen.blit(obslef, (obstacle[0], obstacle[1]))
    pygame.display.flip()


# Manages movements for characters
def Movements():
    global keys, pos
    if keys[0]:
        pos[1] -= 5
        if pos[1] < 110:
            pos[1] = 110
    if keys[2]:
        pos[1] += 5
        if pos[1] >= 850:
            pos[1] = 850
    if keys[1]:
        pos[0] -= 5
        if pos[0] < 0:
            pos[0] = 0
    if keys[3]:
        pos[0] += 5
        if pos[0] > 1860:
            pos[0] = 1860


# Handles how a round should conclude
def Rounds():
    global round_type, pos, speed, alive, game
    if not alive:
        temp = 'You Died'
        text1 = myfont.render(temp, False, (255, 255, 255))
        screen.blit(text1, (825, 300))
        pygame.display.flip()
        sleep(3)
        game = False
        End_Screen()
        return
    if round_type == 0 and alive and pos[1] == 110:
        pos = [900, 110]
        speed = speed * 1.5
        Continue()
    if round_type == 1 and alive and pos[1] == 830:
        pos = [900, 850]
        speed = speed * 1.5
        Continue()


# Calculates score
def Score():
    global time_keeper, round_type, pos, directed_score, score
    if (pygame.time.get_ticks()-time_keeper)/3000 >= 1:
        score -= 1
        time_keeper = pygame.time.get_ticks()
    if round_type == 0:
        if pos[1] <= (831-(directed_score*60)-40):
            if directed_score % 2 == 0:
                score += 5
            else:
                score += 10
            directed_score += 1
    else:
        if pos[1] >= (170+(directed_score*60)):
            if directed_score % 2 == 0:
                score += 5
            else:
                score += 10
            directed_score += 1


# Checks for collision
def Collisions():
    global pos, fixed_obstacles, moving_obstacles, alive
    # Collision between player and fixed obstacle
    for obstacle in fixed_obstacles:
        if pos[0] > (obstacle[0]*60):
            if pos[0] - (obstacle[0]*60) < 60:
                if pos[1] > (110+obstacle[1]*120):
                    if pos[1] - (110+obstacle[1]*120) < 60:
                        alive = False
                else:
                    if (110+obstacle[1]*120) - pos[1] < 40:
                        alive = False
        else:
            if (obstacle[0]*60) - pos[0] < 40:
                if pos[1] > (110+obstacle[1]*120):
                    if pos[1] - (110+obstacle[1]*120) < 60:
                        alive = False
                else:
                    if (110+obstacle[1]*120) - pos[1] < 40:
                        alive = False
    # Collision between player and moving obstacle
    for obstacle in moving_obstacles:
        if pos[0] > obstacle[0]:
            if pos[0] - obstacle[0] < 80:
                if pos[1] > obstacle[1]:
                    if pos[1] - obstacle[1] < 60:
                        alive = False
                else:
                    if obstacle[1] - pos[1] < 40:
                        alive = False
        else:
            if obstacle[0] - pos[0] < 40:
                if pos[1] > obstacle[1]:
                    if pos[1] - obstacle[1] < 60:
                        alive = False
                else:
                    if obstacle[1] - pos[1] < 40:
                        alive = False


# Prints the end screen for displaying the stats of the game
def End_Screen():
    global score
    screen.fill(0)
    temp2 = "Your Score is"
    temp3 = str(score)
    text2 = myfont2.render(temp2, False, (255, 255, 255))
    text3 = myfont2.render(temp3, False, (255, 255, 255))
    temp = "Press any key to return to the main menu"
    text4 = myfont2.render(temp, False, (255, 255, 255))
    temp = "Thanks for playing the game"
    text5 = myfont2.render(temp, False, (255, 255, 255))
    screen.blit(text2, (860, 250))
    screen.blit(text3, (940, 300))
    screen.blit(text4, (710, 350))
    screen.blit(text5, (760, 400))
    pygame.display.flip()
    endg = False
    while not endg:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit(0)
            if event.type == pygame.KEYDOWN:
                endg = True


# Execution starts from here after being called from main
Instruction_Screen()
# Loop used for continous repetition of individual game characters
while game:
    Background()
    Events()
    Moving_Obstacles()
    Collisions()
    Movements()
    Score()
    Rounds()
End_Screen()

