import config

# Setting up all global variables
# Pygame not defined as this file will be executed by main menu
# enviroment_type, player1_type, player2_type, game type  defined in main menu
myfont = pygame.font.SysFont(config.myfont2[0], config.myfont2[1])
Continue = False
# This is the loop for selecting the background
while not Continue:
    screen.fill(0)
    temp = 'Select the enviroment type'
    textsurface1 = myfont.render(temp, False, (255, 255, 255))
    temp = 'L for next, j for previous, k for selecting'
    textsurface2 = myfont.render(temp, False, (255, 255, 255))
    textsurface3 = myfont.render('Voids', False, (255, 255, 255))
    textsurface4 = myfont.render('Partitions', False, (255, 255, 255))
    textsurface5 = myfont.render('Fixed obstacles', False, (255, 255, 255))
    textsurface6 = myfont.render('Moving obstacles', False, (255, 255, 255))
    void = pygame.image.load(config.void[enviroment_type])
    partition = pygame.image.load(config.partition[enviroment_type])
    fixobs = pygame.image.load(config.fixobs[enviroment_type])
    obsrig = pygame.image.load(config.obsrig[enviroment_type])
    screen.blit(textsurface1, (700, 200))
    screen.blit(textsurface2, (640, 250))
    screen.blit(textsurface3, (50, 300))
    screen.blit(textsurface4, (550, 300))
    screen.blit(textsurface5, (1050, 300))
    screen.blit(textsurface6, (1650, 300))
    screen.blit(void, (55, 350))
    screen.blit(partition, (590, 350))
    screen.blit(fixobs, (1120, 350))
    screen.blit(obsrig, (1720, 350))
    pygame.display.flip()
    for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit(0)
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_j:
                    temp = (enviroment_type+len(config.void)-1)
                    temp = temp % len(config.void)
                    enviroment_type = temp
                if event.key == pygame.K_l:
                    enviroment_type = (enviroment_type+1) % len(config.void)
                if event.key == pygame.K_k:
                    Continue = True

Continue = False
# This is the loop for selecting player 1
while not Continue:
    screen.fill(0)
    temp = 'Select player'
    if game_type != 1:
        temp += '1'
    textsurface1 = myfont.render(temp, False, (255, 255, 255))
    temp0 = 'L for next, j for previous, k for selecting'
    textsurface2 = myfont.render(temp0, False, (255, 255, 255))
    textsurface3 = myfont.render(temp, False, (255, 255, 255))
    player = pygame.image.load(config.player[player1_type])
    screen.blit(textsurface1, (860, 200))
    screen.blit(textsurface2, (680, 250))
    screen.blit(textsurface3, (860, 310))
    screen.blit(player, (930, 350))
    pygame.display.flip()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit(0)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_j:
                player1_type = (player1_type + len(config.player)-1)
                player1_type %= len(config.player)
            if event.key == pygame.K_l:
                player1_type = (player1_type + 1) % len(config.player)
            if event.key == pygame.K_k:
                Continue = True

Continue = False
# This is the loop for selecting player 2
while not Continue and not game_type == 1:
    screen.fill(0)
    temp = 'Select player2'
    textsurface1 = myfont.render(temp, False, (255, 255, 255))
    temp0 = 'L for next, j for previous, k for selecting'
    textsurface2 = myfont.render(temp0, False, (255, 255, 255))
    textsurface3 = myfont.render(temp, False, (255, 255, 255))
    if player1_type == player2_type:
        player2_type += 1
    player = pygame.image.load(config.player[player2_type])
    screen.blit(textsurface1, (860, 200))
    screen.blit(textsurface2, (680, 250))
    screen.blit(textsurface3, (860, 310))
    screen.blit(player, (930, 350))
    pygame.display.flip()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit(0)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_j:
                player2_type = (player2_type + len(config.player)-1)
                player2_type %= len(config.player)
                if player1_type == player2_type:
                    player2_type = (player2_type + len(config.player)-1)
                player2_type %= len(config.player)

            if event.key == pygame.K_l:
                player2_type = (player2_type + 1) % len(config.player)
                if player1_type == player2_type:
                    player2_type = (player2_type + 1) % len(config.player)
            if event.key == pygame.K_k:
                Continue = True

