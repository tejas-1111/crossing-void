# Crossing Void  
### My version of the classic game crossing road  
  
  This respository contains the game I have developed for my Spring 2020 course,
  Introduction to Software Systems.
    
  I have created a 1920 x 1080 version of the game

  All the codes follow Pep-8 standards

  All the instructions are always displayed before the game begins

  To run the game, go inside the directory, and then inside pyfiles, and then run main_menu.py using Python3